import LoginLayout from '@/layouts/Login'
import MainLayout from '@/layouts/Main'
import store from '@/store'
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      redirect: 'dashboard',
      component: MainLayout,
      meta: {
        authRequired: true,
        hidden: true,
      },
      children: [
        {
          path: '/dashboard',
          meta: {
            title: 'Dashboard',
          },
          component: () => import('./views/dashboard'),
        },
        {
          path: '/news',
          meta: {
            title: 'News',
          },
          component: () => import('./views/news'),
        },
        {
          path: '/strategy',
          meta: {
            title: 'strategy',
          },
          component: () => import('./views/strategy'),
        },
        {
          path: '/strategy/portfolio',
          meta: {
            title: 'portfolio',
          },
          component: () => import('./views/strategy/portfolio'),
        },
        {
          path: '/strategy/networking',
          meta: {
            title: 'networking',
          },
          component: () => import('./views/strategy/networking'),
        },
        {
          path: '/catalogs/use-cases',
          meta: {
            title: 'Use cases',
          },
          component: () => import('./views/catalogs/use-cases'),
        },
        {
          path: '/catalogs/use-cases/new',
          meta: {
            title: 'New use cases',
          },
          component: () => import('./views/catalogs/use-cases/new'),
        },
        {
          path: '/catalogs/use-cases/:id',
          meta: {
            title: 'Use cases',
          },
          component: () => import('./views/catalogs/use-cases/show'),
        },
        {
          path: '/catalogs/use-cases/:sector/:usecase',
          meta: {
            title: 'Pattern detail',
          },
          component: () => import('./views/catalogs/use-cases/show'),
        },
        {
          path: '/catalogs/products',
          meta: {
            title: 'products',
          },
          component: () => import('./views/catalogs/products'),
        },
        {
          path: '/catalogs/products/:sector/:product',
          meta: {
            title: 'product detail',
          },
          component: () => import('./views/catalogs/products/show'),
        },
        {
          path: '/catalogs/patterns',
          meta: {
            title: 'Pattern',
          },
          component: () => import('./views/catalogs/patterns'),
        },
        {
          path: '/catalogs/patterns/:area/:task',
          meta: {
            title: 'Pattern detail',
          },
          component: () => import('./views/catalogs/patterns/show'),
        },
        {
          path: '/catalogs/patterns/computer-vision',
          meta: {
            title: 'Computer Vision',
          },
          component: () => import('./views/catalogs/patterns/computer-vision'),
        },

/*
        {
          path: '/catalogs/patterns/movie-review',
          meta: {
            title: 'Movie Review',
          },
          component: () => import('./views/catalogs/patterns/movie-review'),
        },


*/



        {
          path: '/catalogs/data',
          meta: {
            title: 'Data',
          },
          component: () => import('./views/catalogs/data'),
        },
        {
          path: '/projects',
          meta: {
            title: 'projects',
          },
          component: () => import('./views/projects'),
        },
        {
          path: '/implementation',
          meta: {
            title: 'implementation',
          },
          component: () => import('./views/implementation'),
        },
        {
          path: '/improvement',
          meta: {
            title: 'improvement',
          },
          component: () => import('./views/improvement'),
        },

        {
          path: '/404',
          meta: {
            title: '404',
          },
          component: () => import('./views/404'),
        },
      ],
    },

    // System Pages
    {
      path: '/user',
      component: LoginLayout,
      redirect: '/user/login',
      children: [
        {
          path: '/user/login',
          meta: {
            title: 'Login',
          },
          component: () => import('./views/user/login'),
        },
        {
          path: '/user/forgot',
          meta: {
            title: 'Forgot Password',
          },
          component: () => import('./views/user/forgot'),
        },
        {
          path: '/user/register',
          meta: {
            title: 'Register',
          },
          component: () => import('./views/user/register'),
        },
      ],
    },

    // Redirect to 404
    {
      path: '*', redirect: '/404', hidden: true,
    },
  ],
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.authRequired)) {
    if (!store.state.user.user) {
      next({
        path: '/user/login',
        query: { redirect: to.fullPath },
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
