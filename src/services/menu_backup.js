/*
const menu = [
  {
    title: 'Dashboard',
    key: 'dashboard',
    url: '/dashboard',
    icon: 'icmn icmn-equalizer',
  },
  {
    divider: true,
  },
  {
    title: 'Strategy',
    key: 'strategy',
    icon: 'icmn icmn-newspaper',
  },
  {
    title: 'Portfolio',
    key: 'portfolio',
    url: '/strategy/portfolio',
  },
  {
    title: 'Networking',
    key: 'networking',
    url: '/strategy/networking',
  },
  {
    divider: true,
  },
  {
    title: 'Use cases',
    key: 'use-cases',
    icon: 'icmn icmn-briefcase',
    url: '/catalogs/use-cases',
  },
  {
    title: 'Patterns',
    key: 'patterns',
    icon: 'icmn icmn-briefcase',
    url: '/catalogs/patterns',
  },
  {
    title: 'Data',
    key: 'data',
    icon: 'icmn icmn-briefcase',
    url: '/catalogs/data',
  },
  {
    divider: true,
  },
  {
    title: 'Lighthouse projects',
    key: 'projects',
    url: '/projects',
    icon: 'icmn icmn-rocket',
  },
  {
    divider: true,
  },
  {
    title: 'Implementation',
    key: 'implementation',
    url: '/implementation',
    icon: 'icmn icmn-power',
  },
  {
    divider: true,
  },
  {
    title: 'Improvement',
    key: 'improvement',
    url: '/improvement',
    icon: 'icmn icmn-spinner2',
  },
  {
    divider: true,
  },
  {
    title: 'Admin',
    key: 'admin',
    url: '/admin',
    icon: 'icmn icmn-cog',
  },
  {
    title: 'Settings',
    key: 'settings',
    icon: 'icmn icmn-cog',
  },
]
*/
