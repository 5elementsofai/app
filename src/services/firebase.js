import store from '@/store'
import router from '@/router'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/storage'

const config = {
  apiKey: 'AIzaSyAGRckLkNUiSRiGzZMX56aqQQtMR8PnvpA',
  databaseURL: 'https://elementsofai-5a8ff.firebaseio.com',
  authDomain: 'elementsofai-5a8ff.firebaseapp.com',
  projectId: 'elementsofai-5a8ff',
  messagingSenderId: '473196343836',
  storageBucket: 'elementsofai-5a8ff.appspot.com',
}

export default {
  install: (Vue, options) => {
    const firebaseApp = firebase.initializeApp(config)
    const auth = firebaseApp.auth()
    Vue.prototype.$auth = {
      login: async (username, pass) => {
        return auth.signInWithEmailAndPassword(username, pass)
      },
      logout: async () => {
        router.push('/user/login')
        await auth.signOut()
      },
    }
    auth.onAuthStateChanged(user => {
      store.commit('UPDATE_USER', { user })
    })
  },
}
