const allowedModules = '*'

const menu = [
  {
    title: 'Solutions',
    key: 'products',
    icon: 'icmn icmn-rocket',
    url: '/catalogs/products',
  },
  {
    title: 'Use cases',
    key: 'use-cases',
    icon: 'icmn icmn-hammer',
    url: '/catalogs/use-cases',
  },
  {
    title: 'Patterns',
    key: 'patterns',
    icon: 'icmn icmn-briefcase',
    url: '/catalogs/patterns',
  },
  /*
  {
    title: 'Movie Review',
    key: 'movie-review',
    icon: 'icmn icmn-briefcase',
    url: '/catalogs/patterns/movie-review',
  },
  /*
  {
    divider: true,
  },
  {
    title: 'Admin',
    key: 'admin',
    url: '/admin',
    icon: 'icmn icmn-cog',
  },
  {
    title: 'Settings',
    key: 'settings',
    icon: 'icmn icmn-cog',
  },
  {
    title: 'User',
    key: 'user',
    icon: 'icmn icmn-user',
    children: [
      {
        title: 'Profile',
        key: 'user',
        url: '/users/profile',
      },
      {
        title: 'Logout',
        key: 'user',
        url: '/users/logout',
      },
    ],
  },
  */
]

function isAuthorizedModule(module) {
  if (allowedModules === '*') {
    return true
  }

  return allowedModules.indexOf(module.key) > -1
}

export const getLeftMenuData = menu.filter(isAuthorizedModule)
export const getTopMenuData = menu.filter(isAuthorizedModule)
